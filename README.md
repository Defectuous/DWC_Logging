############################################
#pi_dwc_monitor
############################################
# Deep Water Culture Hydroponics Monitor
# By: Krowez ( http://www.GravityGreens.us )
############################################
# Monitors:
# PH, PPM, Temprature (Air & Water)
# Logs Results into comma deliated format
# Date, Time, PH, PPM, Water Temp, Air Temp
# 06082018, 0416PM, 5.2, 450, 55w, 72a
############################################